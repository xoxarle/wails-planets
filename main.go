package main

import (
	"embed"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/wailsapp/wails/v2"
	"github.com/wailsapp/wails/v2/pkg/options"
	"github.com/wailsapp/wails/v2/pkg/options/assetserver"
	"github.com/wailsapp/wails/v2/pkg/options/linux"
)

//go:embed all:frontend/dist
var assets embed.FS

var fs http.Handler

func FsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.URL.Path, "/XXX/") {
			ex, err := os.Executable()
			if err != nil {
				panic(err)
			}
			exPath := filepath.Dir(ex)
			fmt.Println(exPath)
			newName := strings.Replace(r.URL.Path, "/XXX", exPath, -1)
			fmt.Print(newName)
			log.Printf("Executing Middleware for %s\n", newName)
			if _, err := os.Stat(newName); err == nil {
				bytes, errf := os.ReadFile(newName)
				if errf == nil {
					rw.Write(bytes)
					return
				} else {
					next.ServeHTTP(rw, r)

				}
			} else {
				next.ServeHTTP(rw, r)
			}
		}
		next.ServeHTTP(rw, r)
	})
}

func main() {
	// Create an instance of the app structure
	app := NewApp()
	fs = http.FileServer(http.Dir("/home/max"))
	http.Handle("/YYY/", fs)

	// err := http.ListenAndServe(":3002", nil)

	// Create application with options
	err := wails.Run(&options.App{
		Title:  "solsys",
		Width:  1024,
		Height: 768,
		AssetServer: &assetserver.Options{
			Assets:     assets,
			Middleware: FsMiddleware,
			Handler:    fs,
		},
		BackgroundColour: &options.RGBA{R: 27, G: 38, B: 54, A: 1},
		OnStartup:        app.startup,
		Bind: []interface{}{
			app,
		},
		Debug: options.Debug{
			OpenInspectorOnStartup: true,
		},
		Linux: &linux.Options{
			//Icon: icon,
			WindowIsTranslucent: false,
			WebviewGpuPolicy:    linux.WebviewGpuPolicyAlways,
		},
	})

	if err != nil {
		println("Error:", err.Error())
	}
}
