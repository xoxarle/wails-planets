import * as THREE from 'three'
import { Trackball } from './TrackballControls';
import { Detector} from './Detector'
//import {OrbitControls} from 'three'

const ROTATION_STEP = 0.003


class PlanetViewer {

	diffuseMap = '/XXX/out/planet_Image.png';
	bumpMap = '/XXX/out/planet_ImageBump.png';
	specMap = '/XXX/out/planet_ImageSpec.png';
	cloudMap = '/XXX/out/planet_ImageCloud.png';
	

	constructor (elemId) {
		this.webglEl = document.getElementById(elemId);


		this.textureLoader = new THREE.TextureLoader()
	
		const dec = new Detector();
		console.log("Detector:\n",dec)
		if (!dec.webgl) {
			console.log ("Ciccio, webgl non è abilitato")
			dec.addGetWebGLMessage(this.webglEl);
			return
		}
		// if (!Detector.webgl) {
		// 	Detector.addGetWebGLMessage(webglEl);
		// 	return;
		// }
	
		this.elemId = elemId
	
		// Earth params
		this.radius   = 0.5,
		this.segments = 64,
		this.rotation = 6;  
	
		this.width  = this.webglEl.clientWidth;
		this.height = this.webglEl.clientHeight;

		this.scene = new THREE.Scene();
	
		this.camera = new THREE.PerspectiveCamera(45, this.width / this.height, 0.01, 1000);
		this.camera.position.z = 1.5;
		
		this.createObjects()

		//window.addEventListener('resize', this.resize, false);
		console.log("Rendering first...")
		this.render()
		console.log("Rendered first...")
		console.log(this.webglEl.clientLeft)
	}

	createObjects() {
		this.renderer = new THREE.WebGLRenderer();
		this.renderer.setSize(this.width, this.height);
	
		this.scene.add(new THREE.AmbientLight(0x55555));
	
		this.light = new THREE.DirectionalLight(0xffffff, 1);
		this.light.position.set(5,3,5);
		this.scene.add(this.light);
	
		this.sphere = this.createSphere(this.radius, this.segments);
		this.sphere.rotation.y = this.rotation; 
		this.scene.add(this.sphere)
	
		this.clouds = this.createClouds(this.radius, this.segments);
		this.clouds.rotation.y = this.rotation;
		this.scene.add(this.clouds)
	
		var stars = this.createStars(90, 64);
		this.scene.add(stars);
	
		//var controls = new TrackballControls(camera);
	
		this.webglEl.appendChild(this.renderer.domElement);
		this.controls = new Trackball( this.camera, this.renderer.domElement );
		

	}

	resize() {
		this.width  = window.innerWidth  //this.webglEl.clientWidth;
		this.height = window.innerHeight // - (window.document.getElementById("toolbar").clientHeight)
		console.log(`${this.width} | ${this.height}`)
		this.renderer.setSize(this.width, this.height);
		this.renderer.domElement.style.height = `${this.height}px`
		this.camera.aspect = this.width / this.height;
		this.camera.updateProjectionMatrix();
		this.resizeCanvasToDisplaySize();
	}

	resizeCanvasToDisplaySize() {
		const canvas = this.renderer.domElement;
		// look up the size the canvas is being displayed
		const width = canvas.clientWidth;
		const height = canvas.clientHeight;
	  
		// adjust displayBuffer size to match
		//if (canvas.width !== width || canvas.height !== height) {
		if (true) {
		  // you must pass false here or three.js sadly fights the browser
		  this.renderer.setSize(width, height, false);
		  this.camera.aspect = width / height;
		  this.camera.updateProjectionMatrix();
	  
		  // update any render target sizes here
		  console.log("Updated resizeCanvasToDisplaySize")
		}
	  }

	generateGuid() {
		var result, i, j;
		result = '';
		for(j=0; j<32; j++) {
		  if( j == 8 || j == 12 || j == 16 || j == 20)
			result = result + '-';
		  i = Math.floor(Math.random()*16).toString(16).toUpperCase();
		  result = result + i;
		}
		return result;
	  }

	render() {
		//controls.update();
		this.sphere.rotation.y += ROTATION_STEP;
		this.clouds.rotation.y += ROTATION_STEP + 0.3 * ROTATION_STEP;		
		requestAnimationFrame(this.render.bind(this))
		this.controls.update()
		//this.resizeCanvasToDisplaySize()
		this.renderer.render(this.scene, this.camera);
		//console.log("done rendering")
	}

	updateSphere() {
		var material = new THREE.MeshPhongMaterial({
			map:         this.textureLoader.load(this.diffuseMap +'?id='+this.generateGuid()),
			bumpMap:     this.textureLoader.load(this.bumpMap +'?id='+this.generateGuid()),
			bumpScale:   0.01,
			specularMap: this.textureLoader.load(this.specMap +'?id='+this.generateGuid()),
			specular:    new THREE.Color('grey')								
		})
		this.sphere.material = material;
	}

	createSphere(radius, segments) {
		return new THREE.Mesh(
			new THREE.SphereGeometry(radius, segments, segments),
			new THREE.MeshPhongMaterial({
				map:         this.textureLoader.load(this.diffuseMap +'?id='+this.generateGuid()),
				bumpMap:     this.textureLoader.load(this.bumpMap +'?id='+this.generateGuid()),
				bumpScale:   0.01,
				specularMap: this.textureLoader.load(this.specMap +'?id='+this.generateGuid()),
				specular:    new THREE.Color('grey')								
			})
		);
	}

	createClouds(radius, segments) {
		return new THREE.Mesh(
			new THREE.SphereGeometry(radius + 0.003, segments, segments),			
			new THREE.MeshPhongMaterial({
				map:         this.textureLoader.load(this.cloudMap+'?id='+this.generateGuid()),
				transparent: true
			})
		);		
	}

	createStars(radius, segments) {
		return new THREE.Mesh(
			new THREE.SphereGeometry(radius, segments, segments), 
			new THREE.MeshBasicMaterial({
				map:  this.textureLoader.load('/XXX/out/galaxy_starfield.png'), 
				side: THREE.BackSide
			})
		);
	}

	handleWindowResize() {
		console.log("Rsiz")
		try {
			var WIDTH = document.getElementById(this.elemId).getBoundingClientRect().width;
		var HEIGHT = document.getElementById(this.elemId).getBoundingClientRect().height;
		console.log(WIDTH, HEIGHT)
	
		this.renderer.setSize(WIDTH, HEIGHT);
		this.camera.aspect = WIDTH / HEIGHT;
		this.camera.updateProjectionMatrix();
		} catch (e) {}
	}
	
	

}

export  {PlanetViewer}
