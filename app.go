package main

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync"

	"codeberg.org/xoxarle/planetnoiselib"
	"github.com/antchfx/jsonquery"
)

// App struct
type App struct {
	ctx           context.Context
	tb            *planetnoiselib.TextureBuilder
	ColorMap      string
	BumpMap       string
	CloudMap      string
	ReflectionMap string
}

// NewApp creates a new App application struct
func NewApp() *App {
	a := App{}
	a.tb, _ = planetnoiselib.NewTextureBuilder()
	return &a
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx

}

// Greet returns a greeting for the given name
func (a *App) Greet(name string) string {
	return fmt.Sprintf("Hello %s, It's show time!", name)
}

func (a *App) GetTemplates() []string {
	w := planetnoiselib.JsonPresets
	sort.Strings(w)
	return w
}

func (a *App) ExtractMainTextures(doc *jsonquery.Node) {
	/*
		    "bumpMap": "normal",
		    "cloudMap": "specular",
		    "colorMap": "image1",
			reflectionMap
	*/
	a.ColorMap = strings.Replace(fmt.Sprintf("%v", jsonquery.FindOne(doc, "colorMap").ChildNodes()[0].Value()), "\n", "", -1)
	a.BumpMap = strings.Replace(fmt.Sprintf("%v", jsonquery.FindOne(doc, "bumpMap").ChildNodes()[0].Value()), "\n", "", -1)
	a.CloudMap = strings.Replace(fmt.Sprintf("%v", jsonquery.FindOne(doc, "cloudMap").ChildNodes()[0].Value()), "\n", "", -1)
	a.ReflectionMap = strings.Replace(fmt.Sprintf("%v", jsonquery.FindOne(doc, "reflectionMap").ChildNodes()[0].Value()), "\n", "", -1)
}

func (a *App) CreatePlanet(name string, x int, y int) []string {
	fmt.Println("Creating planet go")
	mutex := sync.Mutex{}
	mutex.Lock()
	u_id := planetnoiselib.CreateWord()
	a.tb.Free()
	a.tb, _ = planetnoiselib.NewTextureBuilder()

	json, err := planetnoiselib.GetJsonPreset(name)
	if err != nil {
		return []string{"ERROR"}
	}
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	basePath := exPath + "/out/textures/" + u_id
	if _, err := os.Stat(basePath); err != nil {
		err = os.MkdirAll(basePath, 0755)
		if err != nil {
			panic(err)
		}
	} else {
		panic(err)
	}

	err = a.tb.LoadTextureFromJsonString(json)
	if err != nil {
		return []string{"ERROR LOADING TEXTURE"}
	}

	doc, e2 := jsonquery.Parse(strings.NewReader(json))
	if e2 != nil {
		return []string{"CANNOT PARSE JSON"}

	}
	images := jsonquery.FindOne(doc, "images")

	a.tb.SetSize(x, y)
	if err != nil {
		return []string{"ERROR SETTING SIZE"}
	}
	textureName := fmt.Sprintf("%s-%s", u_id, name)
	a.tb.SetOutputName(textureName)
	if err != nil {
		return []string{"ERROR SETTING NAME"}
	}
	fmt.Println("Rendering planet go")
	a.tb.RenderTexture(basePath)
	if err != nil {
		return []string{"ERROR RENDERING TEXTURE"}
	}
	fmt.Println("-- PREPARING DATA -------------------------")

	a.ExtractMainTextures(doc)
	maps := []string{
		a.ColorMap,
		a.BumpMap,
		a.ReflectionMap,
		a.CloudMap,
	}
	fmt.Printf("MAPS ARE: %v\n", maps)

	fmt.Printf("IMAGES ARE: %v\n", images)
	sData := []string{}
	for _, imz := range maps {
		if imz != "" {
			sData = append(sData, strings.Replace(fmt.Sprintf("/XXX/out/textures/%s/%s_%s.png\n", u_id, textureName, imz), "\n", "", -1))
		}
	}
	for _, sx := range sData {
		fmt.Printf("|%s|\n", sx)
	}
	fmt.Printf("-- DONE DATA %s %v\n", textureName, sData)
	mutex.Unlock()
	return sData
}
